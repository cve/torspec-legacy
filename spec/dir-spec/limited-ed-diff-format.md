<a id="dir-spec.txt-E"></a>

# Limited ed diff format

We support the following format for consensus diffs.  It's a
subset of the ed diff format, but clients MUST NOT accept other
ed commands.

We support the following ed commands, each on a line by itself:

```text
    - "<n1>d"          Delete line n1
    - "<n1>,<n2>d"     Delete lines n1 through n2, inclusive
    - "<n1>,$d"        Delete line n1 through the end of the file, inclusive.
    - "<n1>c"          Replace line n1 with the following block
    - "<n1>,<n2>c"     Replace lines n1 through n2, inclusive, with the
                       following block.
    - "<n1>a"          Append the following block after line n1.
```

Note that line numbers always apply to the file after all previous
commands have already been applied.  Note also that line numbers
are 1-indexed.

The commands MUST apply to the file from back to front, such that
lines are only ever referred to by their position in the original
file.

If there are any directory signatures on the original document, the
first command MUST be a "`<n1>,$d`" form to remove all of the directory
signatures.  Using this format ensures that the client will
successfully apply the diff even if they have an unusual encoding for
the signatures.

The replace and append command take blocks.  These blocks are simply
appended to the diff after the line with the command.  A line with
just a period (".") ends the block (and is not part of the lines
to add).  Note that it is impossible to insert a line with just
a single dot.
