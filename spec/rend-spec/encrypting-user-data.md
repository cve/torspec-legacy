<a id="rend-spec-v3.txt-5"></a>

# Encrypting data between client and host

A successfully completed handshake, as embedded in the
INTRODUCE/RENDEZVOUS messages, gives the client and hidden service host
a shared set of keys Kf, Kb, Df, Db, which they use for sending
end-to-end traffic encryption and authentication as in the regular
Tor relay encryption protocol, applying encryption with these keys
before other encryption, and decrypting with these keys before other
decryption. The client encrypts with Kf and decrypts with Kb; the
service host does the opposite.
